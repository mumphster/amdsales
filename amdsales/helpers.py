import json
import requests
import re
from unidecode import unidecode
from mailsnake import MailSnake

_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')

def slugify(text, delim=u'-'):
    """Generates an ASCII-only slug."""
    result = []
    for word in _punct_re.split(text.lower()):
        result.extend(unidecode(word).split())
    return unicode(delim.join(result))

class MailchimpHelper(object):
    def __init__(self):
        api_key = '74fc2f169f66ff7853b77e22295bde3e-us4'
        api_url = 'http://us4.api.mailchimp.com/1.3/'
        self.ms = MailSnake(api_key)

    def sub_user(self, email):
        merge_vars = {
            'FNAME': 'AMDSales',
            'LNAME': 'AMDSales',
        }
        sub = self.ms.listSubscribe(id='8df315e5e2', email_address=email, merge_vars=merge_vars, update_existing=True, double_optin=False)
        return sub


class FacebookHelper(object):
    def __init__(self):
        self.access_token = 'AAADFSvfD3aYBAMok7zpQWXKloM7ZBcC2b8Dn9QFZBjxwy7Qo6CMhH7SSIiAl9ans0ufxTLmZBE3uZCLnGNbWdkjwFdcv0ngwESu7VXxFqAZDZD'

    def facebook_page(self, endpoint):
        url = 'https://graph.facebook.com/AscensionMD/%s&access_token=%s' % (endpoint, self.access_token)

        r = requests.get(url)
        datas = json.loads(r.content)
        for data in datas['data']:
            date = data['updated_time'].split('T')
            data['post_date'] = date[0]
            data['post_time'] = date[1]
            data['url'] = 'http://facebook.com/' + data['id']
        return datas

    def get_albums(self):
        url = 'https://graph.facebook.com/AscensionMD/albums?access_token=%s' % self.access_token

        r = requests.get(url)
        data = json.loads(r.content)['data']
        d = []
        for x in data:
            if 'count' in x: #filters out weird empty albums
                d.append({'title': x['name'].lower(), 'id': x['id'], 'url': x['link']})
        return d

    def get_photos(self, album_id):
        url = 'https://graph.facebook.com/%s/photos?access_token=%s' % (album_id, self.access_token)
        r = requests.get(url)
        photos = json.loads(r.content)['data']

        images = []

        for x in range(3):
            image = photos[x]['images'][0]['source']
            images.append(image)

        return images

    def get_events(self):
        url = 'https://graph.facebook.com/AscensionMD/events?access_token=%s' % self.access_token

        r = requests.get(url)
        data = json.loads(r.content)['data']
        for x in data:
            time_start = x['start_time'].split('T')
            time_end = x['end_time'].split('T')

            x['start_date'] = time_start[0]
            x['start_time'] = time_start[1]
            x['end_time'] = time_end[1]
            x['end_date'] = time_end[0]
            x['url'] = 'http://facebook.com/%s' % x['id']
            x['event_slug'] = slugify(x['name']) 

        return data


if __name__ == '__main__':
    f = FacebookHelper()
    m = MailchimpHelper()
    #print m.sub_user('cool2@hotmail.com')
    #print f.get_albums()
