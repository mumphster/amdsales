from flask import Blueprint, render_template, abort, request, flash, redirect, jsonify
from amdsales.helpers import FacebookHelper

from amdsales.models import Event

events = Blueprint('events', __name__)
facebook = FacebookHelper()

@events.route('/')
def list():
    events = Event.query.order_by('id').all()
    for e in events:
        e.photos = facebook.get_photos(e.gallery_id)
    return render_template('events/list.html', events=events)