from flask import Blueprint, render_template, abort, jsonify
from amdsales.helpers import FacebookHelper
import json

api = Blueprint('api', __name__)
facebook = FacebookHelper()

@api.route('/facebook/page/<endpoint>')
def facebook_index(endpoint):
    return json.dumps(facebook.facebook_page(endpoint))

@api.route('/facebook/events/')
def facebook_event():
    return json.dumps(facebook.get_events())
