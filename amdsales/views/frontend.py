from flask import Blueprint, render_template, abort, request, flash, redirect, jsonify
from jinja2 import TemplateNotFound
from amdsales.helpers import FacebookHelper, MailchimpHelper
from amdsales.forms import SubscribeForm
import json

frontend = Blueprint('frontend', __name__)
facebook = FacebookHelper()

@frontend.route('/', methods=['GET', 'POST'])
def index(form=None):
    feed = facebook.facebook_page("statuses&limit=3")['data']

    events = facebook.get_events()

    if form == None:
        form = SubscribeForm()

    return render_template('frontend/index.html', feed=feed, events=events, form=form)

@frontend.route('/submit/', methods=['POST',])
def submit():
    form = SubscribeForm(csrf_enabled=False)

    if form.validate_on_submit():
        email = form.email.data

        mc = MailchimpHelper()
        m = mc.sub_user(email)
        
        if m:
            return 'True'
        else:
            return 'False'
        
    else:
        return 'False'

@frontend.route('/about/')
def about():
    return render_template('frontend/about.html')

@frontend.route('/brands/')
def brands():
    return render_template('frontend/brands.html')

@frontend.route('/<page>/')
def show(page):
    try:
        return render_template('frontend/%s.html' % page)
    except TemplateNotFound:
        abort(404)
