from flask import Blueprint, request, render_template, abort, jsonify, session, current_app, redirect, flash, url_for
from amdsales.helpers import FacebookHelper
from amdsales.models import Event
from amdsales.forms import LoginForm, EventForm
from amdsales.extensions import db

admin = Blueprint('admin', __name__)
facebook = FacebookHelper()

USERNAME = 'amdsales'
PASSWORD = 'Ascension2012'

@admin.route('/')
def admin_index():
    if not session.get('logged_in'):
        return redirect('/admin/login/')
    else:
        return render_template('admin/index.html')


@admin.route('/events/')
def event_list():
    if not session.get('logged_in'):
        return redirect('/admin/login/')
    else:
        events = Event.query.order_by('id').all()
        return render_template('admin/event_list.html', events=events)

@admin.route('/event/<event_id>/', methods=['GET', 'DELETE'])
def event_index(event_id=None):
    if not session.get('logged_in'):
        return redirect('/admin/login/')
    else:
        if request.method == "GET":
            event = Event.query.filter_by(id=event_id).first()
            return render_template('admin/event.html', event=event)
        elif request.method == "DELETE":
            event = Event.query.filter_by(id=event_id).first()
            db.session.delete(event)
            db.session.commit()
            return '200'

@admin.route('/event/add/', methods=['GET', 'POST'])
def event_add():
    form = EventForm()
    if not session.get('logged_in'):
        return redirect('/admin/login/')
    else:
        albums = facebook.get_albums()
        form.gallery_url.choices = [(g['url'], g['title']) for g in albums]

        if form.validate_on_submit():
            event = Event(form.title.data, form.content.data, form.gallery_url.data)
            db.session.add(event)
            db.session.commit()
            return redirect('/admin/events/')

        return render_template('admin/event_add.html', form=form, albums=albums)

@admin.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginForm(csrf_enabled=False)

    if form.validate_on_submit():
        session['logged_in'] = True
        flash('You were logged in')
        return redirect('/admin/')

    return render_template('admin/login.html', form=form)

@admin.route('/logout/')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect('/admin/')
