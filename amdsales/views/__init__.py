from .frontend import frontend
from .events import events
from .api import api
from .admin import admin
