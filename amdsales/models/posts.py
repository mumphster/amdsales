from amdsales.extensions import db
from amdsales.helpers import FacebookHelper
from urlparse import urlparse

facebook = FacebookHelper()

class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(40))
    content = db.Column(db.Text())
    gallery_url = db.Column(db.String(200))
    gallery_id = db.Column(db.Integer)

    def __init__(self, title, content, gallery_url):
        self.title = title
        self.content = content
        self.gallery_url = gallery_url

        # lol url parsing.
        self.gallery_id = dict([part.split('=') for part in urlparse(gallery_url)[4].split("&")])['fbid']

    def __repr__(self):
        return '<Event: %r>' % self.title
