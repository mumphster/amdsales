from flaskext.wtf import Form, TextField, SelectField, email, ValidationError, PasswordField, required, SubmitField
import requests
from amdsales.helpers import FacebookHelper


class EventForm(Form):
    title = TextField('Event Title', validators=[required(message="You must enter a title")])
    content = TextField("Description", validators=[required(message="You must enter some content")])
    gallery_url = SelectField("Event Gallery Name")
    submit = SubmitField("Create")
