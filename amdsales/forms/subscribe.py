from flaskext.wtf import Form, TextField, email, ValidationError, PasswordField, required, SubmitField


class SubscribeForm(Form):
    email = TextField('Email', validators=[email(message="You must enter an Email")])
    submit = SubmitField("Submit")
