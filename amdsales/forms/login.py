from flaskext.wtf import Form, TextField, email, ValidationError, PasswordField, required, SubmitField

USERNAME = 'amdsales'
PASSWORD = 'Ascension2012'

class LoginForm(Form):
    login = TextField('Username', validators=[required(message="You must enter a username")])
    password = PasswordField("Password")
    submit = SubmitField("Login")

    def validate_login(self, field):
        if field.data != USERNAME:
            raise ValidationError, "Invalid Username"
    
    def validate_password(self, field):
        if field.data != PASSWORD:
            raise ValidationError, "Invalid Passaword"
