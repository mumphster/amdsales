from amdsales import views


class DefaultConfig(object):
    
    SQLALCHEMY_DATABASE_URI = "sqlite:///dev.db"

    DEBUG = True
    SECRET_KEY = 'secret'
    TESTING = True
    CSRF_ENABLED = False
