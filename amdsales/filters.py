import time

def create_filters(app):
    def datetimeformat(value, format_in="%Y-%m-%d", format_out="%B %d, %Y"):
        val_as_time = time.strptime(value, format_in)
        return time.strftime(format_out, val_as_time)

    app.jinja_env.filters['datetimeformat'] = datetimeformat
