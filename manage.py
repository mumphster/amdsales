from flaskext.script import Manager
from amdsales import create_app
from amdsales.extensions import db



manager = Manager(create_app)

@manager.command
def createall():
    "Creates database tables"
    db.create_all()

@manager.command
def dropall():
    "Drops all database tables"
    db.drop_all()


if __name__ == "__main__":
    manager.run()
